"use strict"

/*
- При загрузке страницы показать на ней кнопку с текстом "Нарисовать круг". Данная кнопка единственный контент в HTML документе,
весь остальной контент должен быть создан и добавлен на страницу с помощью JS.
- При нажатии на кнопку "Нарисовать круг" показать одно поле для ввода - диаметр круга. При нажатии на кнопку "Нарисовать"
создать на странице 100 кругов (10х10) случайного цвета. При клике на конкретный круг - этот круг должен исчезать при этом
пустое место заполняется - остальные круги сдвигаются влево.
*/

const btn = document.querySelector(`.btn`);
// Функція на кнопку "Намалювати кільця"
btn.onclick = function() {
    btn.style.display = `none`;

    // Створюємо поле вводу для діаметра
    let inpDiametr = document.createElement(`input`);
    btn.after(inpDiametr);
    inpDiametr.placeholder = "діаметр круга";

    // Створюємо кнопку "Намалювати"
    let btnDrow = document.createElement(`button`);
    btnDrow.innerText = `Намалювати`;
    inpDiametr.after(btnDrow);

    // Зчитуємо введені данні користувачем
    btnDrow.onclick = function() {
        
        let textInp = inpDiametr.value;

        // Створюємо блок кругів
        let blockCircles = document.createElement(`div`);
        // Стиль блока кругів
        blockCircles.style.width = `${textInp*10+60}px`
        blockCircles.style.height = `${textInp*10+100}px`

        // Розташовуємо блок кругів після кнопки намалювати
        btnDrow.after(blockCircles);
        
        let circle = document.createElement(`div`);
        // Намалювали круги
        for(let i = 0; i < 10; i++) {

            for(let j = 0; j < 10; j++) {
                
                circle = document.createElement(`div`);
                // Cтиль круга
                circle.style.display = `inline-block`;
                circle.style.width = `${textInp}px`;
                circle.style.height = `${textInp}px`;
                circle.style.margin = `3px`;
                circle.style.borderRadius = `50%`;
                circle.style.backgroundColor = `hsl(${Math.floor(Math.random()*360)}, 100%, 50%)`;
                // Вставляємо круг на початку блока
                blockCircles.prepend(circle);
            }
        }
        // Видаляємо круги по кліку зі зміщенням вліво
        let [...arrCircles] = document.querySelectorAll(`div > div`);
        arrCircles.forEach(function(div) {
            div.onclick = function() {
                div.remove(); 
            }
        })
    }
}