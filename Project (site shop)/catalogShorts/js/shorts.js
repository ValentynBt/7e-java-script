
import shorts from "./shortsObj.js";

window.addEventListener("DOMContentLoaded", () => {
    // Знаходимо блок пагинації
    let [...pages] = document.querySelectorAll(`.numb-page`);
    // console.log(pages);
    // Знаходимо блок каталога
    let catalog = document.querySelector(`.catalog`);
    // Створюємо першу сторінку каталогу
    for(let i = 1; i <= 9; i++) {
        let card = document.createElement(`div`);
        card.classList.add(`card`);
        catalog.append(card);
        console.log(`card`);
        // Створення контенту картки
        card.insertAdjacentHTML(`beforeend`, `<img src="./images/shorts/short${i}.png" alt="image short">
            <div class="title-shorts">${shorts[i].name}</div>
            <div class="stars">
                <svg value="1" width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path class="star-svg" d="M12.1795 0L15.7587 7.25312L23.7629 8.41582L17.9708 14.0612L19.3384 22.0329L12.1795 18.2688L5.02056 22.0329L6.38819 14.0612L0.596107 8.41582L8.60027 7.25312L12.1795 0Z" fill="#FFCC48"/>
                </svg>
                <svg value="2" width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path class="star-svg" d="M12.1795 0L15.7587 7.25312L23.7629 8.41582L17.9708 14.0612L19.3384 22.0329L12.1795 18.2688L5.02056 22.0329L6.38819 14.0612L0.596107 8.41582L8.60027 7.25312L12.1795 0Z" fill="#FFCC48"/>
                </svg>
                <svg value="3" width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path class="star-svg" d="M12.1795 0L15.7587 7.25312L23.7629 8.41582L17.9708 14.0612L19.3384 22.0329L12.1795 18.2688L5.02056 22.0329L6.38819 14.0612L0.596107 8.41582L8.60027 7.25312L12.1795 0Z" fill="#FFCC48"/>
                </svg>
                <svg value="4" width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path class="star-svg" d="M12.1795 0L15.7587 7.25312L23.7629 8.41582L17.9708 14.0612L19.3384 22.0329L12.1795 18.2688L5.02056 22.0329L6.38819 14.0612L0.596107 8.41582L8.60027 7.25312L12.1795 0Z" fill="#FFCC48"/>
                </svg>
                <svg value="5" width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path class="star-svg" d="M12.1795 0L15.7587 7.25312L23.7629 8.41582L17.9708 14.0612L19.3384 22.0329L12.1795 18.2688L5.02056 22.0329L6.38819 14.0612L0.596107 8.41582L8.60027 7.25312L12.1795 0Z" fill="#FFCC48"/>
                </svg>
            <div>

            `);
        // Рейтинг товару в зірках
        checkStar(card);

    }
    // Кількість створюваних карток с товаром на одну сторінку
    let cardOnPage = 9;
    let pageNumb = 1;
    // Навішуємо подію на кожну кнопку для виводу товару
    for(let page of pages) {

        page.addEventListener(`click`, (e) => {

            pageNumb = e.target.value;
            console.log(pageNumb);
            
            // Створюємо нові масиви на кожну кнопку, розбиваючи загальгий масив товарів(не змінюючи його) на кожну сторінку пагінації
            let start = (pageNumb - 1) * cardOnPage;
            let end = start + cardOnPage;

            let pageShorts = shorts.slice(start, end);
            // Чистимо каталог
            catalog.innerText = ``;
            // Створюємо картки
            for(; start < end; start++) {
                if(start >= shorts.length) {
                    break
                };
                let card = document.createElement(`div`);
                card.classList.add(`card`);
                catalog.append(card);
                card.insertAdjacentHTML(`beforeend`, `<img src="./images/shorts/short${start}.png" alt="image short">
                    <div class="title-shorts">${shorts[start].name}</div>
                    <div class="stars">
                        <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path class="star-svg" d="M12.1795 0L15.7587 7.25312L23.7629 8.41582L17.9708 14.0612L19.3384 22.0329L12.1795 18.2688L5.02056 22.0329L6.38819 14.0612L0.596107 8.41582L8.60027 7.25312L12.1795 0Z" fill="#FFCC48"/>
                        </svg>
                        <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path class="star-svg" d="M12.1795 0L15.7587 7.25312L23.7629 8.41582L17.9708 14.0612L19.3384 22.0329L12.1795 18.2688L5.02056 22.0329L6.38819 14.0612L0.596107 8.41582L8.60027 7.25312L12.1795 0Z" fill="#FFCC48"/>
                        </svg>
                        <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path class="star-svg" d="M12.1795 0L15.7587 7.25312L23.7629 8.41582L17.9708 14.0612L19.3384 22.0329L12.1795 18.2688L5.02056 22.0329L6.38819 14.0612L0.596107 8.41582L8.60027 7.25312L12.1795 0Z" fill="#FFCC48"/>
                        </svg>
                        <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path class="star-svg" d="M12.1795 0L15.7587 7.25312L23.7629 8.41582L17.9708 14.0612L19.3384 22.0329L12.1795 18.2688L5.02056 22.0329L6.38819 14.0612L0.596107 8.41582L8.60027 7.25312L12.1795 0Z" fill="#FFCC48"/>
                        </svg>
                        <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path class="star-svg" d="M12.1795 0L15.7587 7.25312L23.7629 8.41582L17.9708 14.0612L19.3384 22.0329L12.1795 18.2688L5.02056 22.0329L6.38819 14.0612L0.596107 8.41582L8.60027 7.25312L12.1795 0Z" fill="#FFCC48"/>
                        </svg>
                    <div>

                    `);
            // Рейтинг товару в зірках
            checkStar(card);

            }
            // Перенавішуємо атрібут disabled та клас active
            checkDisabled(page);

        })

    }
// ФУНКЦІЇ
    // Функція навішування зірок
    function checkStar(cardStar) {
        let stars = cardStar.querySelector(`.stars`);
        console.log(stars);
        // Навішуємо клік на зірку
        stars.addEventListener(`click`, (e) => {
            let [...targetStar] = stars.querySelectorAll(`.star-svg`);
            for(let item of targetStar) {
                item.style.fill = `#FFCC48`;
            }
            // Знаходимо елемент масиву по якому був клік
            let indx = targetStar.indexOf(e.target);
    
            for(let str = 0; str <= indx; str++) {
                targetStar[str].style.fill = `blue`;
            }
        })
    }
    // Функція навішування атрібуту disabled та класа active на пагінації
    function checkDisabled(numberPage) {
        let btnDis = document.querySelector(`[disabled]`);
        btnDis.removeAttribute(`disabled`);
        btnDis.classList.remove(`active`);
        numberPage.setAttribute(`disabled`, true);
        numberPage.classList.add(`active`);
    }
})
// Об'єкт під Local Storage
const starsUser = {};
