`use strict`

/*
 №_1
Створіть на сторінці div і дайте йому зовнішній відступ 150 пікселів. Використовуючи JS виведіть у консоль відступ
 №_2
Створіть програму секундомір.
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
* При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий *
 Виведення лічильників у форматі ЧЧ:ММ:СС
* Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/

// №_1
/*
const div = document.createElement("div");
document.body.prepend(div); 
console.log(getComputedStyle(div).margin);
*/

// №_2
const btnStart = document.getElementById("start"), 
    btnStop = document.getElementById("stop"), 
    btnReset = document.getElementById("reset"),
    sec = document.getElementById("sec"),
    min = document.getElementById("min"),
    hour = document.getElementById("hour");

const stopwatch = document.querySelector(".container-stopwatch");

let second = 0,
    minutes = 0,
    hours = 0;
let intervalHandler;

// мітки для кнопок
let clickStart = false,
    clickStop = false,
    clickReset = false;

// Змінюємо фон секундоміра
function clearClass() {
    stopwatch.classList.remove("red");
    stopwatch.classList.remove("black");
    stopwatch.classList.remove("green");
    stopwatch.classList.remove("silver");
}
// Функція для деактивації клікнутих кнопок
function activeButton(click1, click2, click3) {
    if(click1) {
        btnStart.setAttribute(`disabled`, true);
        btnStop.removeAttribute(`disabled`, true);
        btnReset.removeAttribute(`disabled`, true);
    } else if(click2) {
        btnStop.setAttribute(`disabled`, true);
        btnStart.removeAttribute(`disabled`, true);
        btnReset.removeAttribute(`disabled`, true);
    } else if(click3) {
        btnReset.setAttribute(`disabled`, true);
        btnStart.removeAttribute(`disabled`, true);
        btnStop.removeAttribute(`disabled`, true);
    }
}
// Кнопка старт
btnStart.onclick = () => {
    clearClass();
    stopwatch.classList.add("green");
    
    activeButton(clickStart = true, clickStop = false, clickReset = false);
    intervalHandler = setInterval(() => {
        // секунди
        second++
        if(second === 60) {
            second = 0;
            // хвилини
            minutes++;
            if(minutes === 60) {
                minutes = 0;
                // години
                hours++;
                if(hours === 24) {
                    hours = 0;
                }
                if(hours/10 < 1) {
                    hour.innerText = `0` + hours;
                } else {
                    hour.innerText = hours;
                }
            }
            if(minutes/10 < 1) {
                min.innerText = `0` + minutes;
            } else {
                min.innerText = minutes;
            }
        }
        if(second/10 < 1) {
            sec.innerText = `0` + second;
        } else {
            sec.innerText = second;
        }

    }, 1000);
}
// Кнопка стоп
btnStop.onclick = () => {
    clearClass();
    stopwatch.classList.add("red");
    
    activeButton(clickStart = false, clickStop = true, clickReset = false);
    clearInterval(intervalHandler);
}
// Кнопка ресет
btnReset.onclick = () => {
    clearClass();
    stopwatch.classList.add("silver"); 
    
    activeButton(clickStart = false, clickStop = false, clickReset = true);
    clearInterval(intervalHandler);
    second = 0,
    minutes = 0,
    hours = 0;
    sec.innerText = `0` + second;
    min.innerText = `0` + minutes;
    hour.innerText = `0` + hours;
}

/*№_3
Створіть програму перевірки телефону
* Створіть поле для введення телефону та кнопку збереження
* Користувач повинен ввести номер телефону у форматі 000-000-00-00
* Після того як користувач натискає кнопку "зберегти" перевірте правильність введення номера, якщо номер правильний
 зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку 
 https://www.meme-arsenal.com/create/meme/6244708 , якщо буде помилка відобразіть її в div до input
*/
// №_3
const inpTel = document.querySelector(`.tell`),
    btnSaveTel = document.querySelector(`.btn-save`),
    divForm = document.querySelector(`.form`),
// Створюємо змінну для помилки
    err = document.createElement(`p`);

btnSaveTel.onclick = () => {
    // Зчитуємо введені данні користувачем
    let textTel = inpTel.value;
    // Створюємо регулярний вираз
    let regExp = /\d{3}-\d{3}-\d{2}-\d{2}/;
    // Перевіряємо введені дані на регулярний вираз
        chek = textTel.match(regExp);
    
    // Перевірка введеного номера
    if(chek) {
        inpTel.style.backgroundColor = `rgba(39, 211, 76, 0.5)`;
        alert(`Ваш номер телефону: ${chek}`);
        setTimeout(() => {
            document.location = `https://www.meme-arsenal.com/create/meme/6244708`;
        }, 300);
    } else {
        err.style.backgroundColor = `red`;
        err.textContent = `ERROR!!!`;
        inpTel.before(err);
        alert(`Введіть номер телефону \n у форматі: 000-000-00-00`);
    }
}
// Видаляємо параграф помилки при фокусі на input
inpTel.onfocus = () => {
    err.remove();
}

/* №_3
Слайдер
Створіть слайдер кожні 3 секунди змінюватиме зображення*/

// №_3
let zedIndex = 0,
    timeSlaid;
// Шукаємо елементи(картинки і блок) на сторінці
const [...slaider] = document.querySelectorAll(`img`);
const blockSlaid = document.querySelector(`.slaider`);
// При загрузці сторінки запускаєм слайдер
window.onload = () => {
    // Слайдер з інтервалом 3 секунди
    timeSlaid = setInterval(() => {
        zedIndex++;
        if(zedIndex === 3) {
            zedIndex = 0;
        }
        blockSlaid.prepend(slaider[zedIndex]);

    }, 3000);

}




