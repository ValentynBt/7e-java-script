"use strict"

/*
Створити клас Car , Engine та Driver.
* Клас Driver містить поля - ПІБ, стаж водіння.
* Клас Engine містить поля – потужність, виробник.
* Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine, 
методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося",
"Поворот праворуч" або "Поворот ліворуч". А також метод toData(), який виводить 
повну інформацію про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
*/

// Створення класів
class Car {
    constructor(options) {
        this.brand = options.brand;
        this.classauto = options.classauto;
        this.weight = options.weight;
    }
    start() {
        alert(`Поїхали`);
    }
    stop() {
        alert(`Зупиняємося`);
    }
    turnRight() {
        alert(`Поворот праворуч`);
    }
    turnLeft() {
        alert(`Поворот ліворуч`);
    }
    toData() {
        document.write(`<br> Водій: ${driver.name} <br> Стаж, lvl: ${driver.experience} <br>`);
        document.write(`Марка авто: ${car.brand} <br> Класифікація авто: ${car.classauto} <br> Вага автомобіля, kg: ${car.weight} <br>`);
        document.write(`Двигун, h/p: ${engine.power} <br> Виробник: ${engine.manufacturer} <br>`);
    }
}
class Driver {
    constructor(options) {
        this.name = options.name;
        this.experience = options.experience;
    }
}
class Engine {
    constructor(options) {
        this.power = options.power;
        this.manufacturer = options.manufacturer;
    }
}
// Похідні від класу Car
class Lorry extends Car {
    constructor(options) {
        super(options);
        this.carrying = options.carrying;
    }
}
class SportCar extends Car {
    constructor(options) {
        super(options);
        this.speed = options.speed;
    }
}
// визов класів
let car = new Car({
    brand: `Tesla`,
    classauto: `passenger car`,
    weight: 500,
})
let driver = new Driver({
    name: `Vin Disel`,
    experience: 80,
})
let engine = new Engine({
    power: 200,
    manufacturer: `Tesla-Motors`,
})

let lorry = new Lorry({
    brand: `Ford`,
    classauto: `lorry`,
    weight: 300,
    carrying: 400,
})
let sportCar = new SportCar({
    brand: `Formula-1`,
    classauto: "sport car",
    weight: 150,
    speed: 200,
})
car.toData();
// кнопки водіння
const btnGo = document.getElementById(`go`);
const btnStop = document.getElementById(`stop`);
const btnLeft = document.getElementById(`to-left`);
const btnRight = document.getElementById(`to-right`);
btnGo.onclick = car.start;
btnStop.onclick = car.stop;
btnRight.onclick = car.turnRight;
btnLeft.onclick = car.turnLeft;

/*
Все ок, лиш варто було наслідування ще таке зробити:
/1/class Driver {
constructor(fullName, experience){
this.fullName = fullName;
this.experience = experience;
}
}

/2/class Engine extends Driver {
constructor(power, producer, fullName, experience){
super(fullName, experience);
this.power = power;
this.producer = producer;
}
}

/3/class Car extends Engine {
constructor(brand, classC, weight, power, producer, fullName, experience){
super(power, producer, fullName, experience);
this.brand = brand;
this.classC = classC;
this.weight = weight;
}
*/