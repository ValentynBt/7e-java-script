`use strict`

/* 1
Створіть клас, який буде створювати користувачів з ім'ям та фамілією. Додайте до класу метод для виводу ім'я та фамілії */
class User {
    constructor(name, lastName) {
        this.name = name;
        this.lastName = lastName;
    }
    sayHi() {
        alert(`Привіт, ${this.name} ${this.lastName}, гарного дня!`)
    }
}
let userOne = new User(`Валентин`, 'Криптонюк');

/* 2 
Створіть список в якому 4 лішки. Використоауючи js звернися до 2 li. Використовуючи навігацію по DOM дай 1 елементу
синій фон, а 3 - червоний. */
const [...itemList] = document.getElementsByTagName(`li`);
itemList[1].innerText += ` Звернувся до 2го елементу`;
itemList[0].style.background = `hsla(196, 100%, 50%, 1)`;
itemList[2].style.background = `hsla(49, 100%, 50%, 1)`;

/* 3
Створи div висотою 400px і додай на нього подію наведення миші. При наведенні миші виведіть текстом координати
де знаходиться курсор миші. */
const evtMouse = document.querySelector(`.div3`);
evtMouse.addEventListener(`mouseover`, (e) => {
    alert(`Координата X: ${e.clientX} \n Координата Y: ${e.clientY}`);
})

/* 4
Створи кнопки 1,2,3,4 і при натисканні на кнопки виведи інформацію яка кнопка була натиснута */
const [...buttons] = document.querySelector(`#buttons`);
buttons.forEach(function(element) {
    element.addEventListener(`click`, (e) => {
        alert(`Була натиснута кнопка ${e.target.innerText}`);
    })
})

/* 5
Створи div, при наведені мишкою на нього він повинен змінювати своє положення */
const moveDiv = document.querySelector(`.div5`);
moveDiv.addEventListener(`mouseover`, (e) => {
    e.target.style.position = `absolute`;
    e.target.style.bottom = `${Math.floor(Math.random()*90)}%`;
    e.target.style.right = `${Math.floor(Math.random()*90)}%`;
})

/* 6
Створи поле для вводу кольору. коли користувач вибере якийсь колір, зроби його фоном body */
const inpColor = document.querySelector(`.inp-color`),
    btnColor = document.querySelector(`.btn-color`),
    divColor = document.querySelector(`.div-color`);

btnColor.addEventListener(`click`, () => {
    divColor.style.backgroundColor = `hsla(${inpColor.value}, 100%, 50%, 1)`;
})

/* 7
Створи поле для вводу логіна, коли користувач почне вводити дані в поле - виводь їх в консоль */
const inpLog = document.querySelector(`.inp-log`),
    spanInp = document.querySelector(`.span-inp`);

    inpLog.addEventListener(`input`, () => {
    spanInp.innerText = inpLog.value;
})

/* 8
Створіть поле для вводу данних і виведіть текст під полем */
const inpText = document.querySelector(`.inp-text`),
    btnText = document.querySelector(`.btn-text`),
    div8 = document.querySelector(`.task8`),
    p1 = document.createElement(`p`);
    
btnText.addEventListener(`click`, () => {
    div8.append(p1);
    p1.innerText = inpText.value;
})