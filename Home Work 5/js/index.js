"use strict"

// Завдання 1
// Напишіть функцію isEmpty(obj), яка повертає true, якщо об'єкт не має властивостей, інакше false.

let obj = {
    // name: "Jora",
    // age: 12,
}
function isEmpty(anyObject) {
    return Object.keys(anyObject).length == 0 ? "true" : "false";
}
console.log(isEmpty(obj));

// Завдання 2-3

// Створили функцію-конструктор
function Human(age, name) {
    this.name = name;
    // Метод в конструкторі (статичний метод)
    Human.say = function() {
        console.log(`Hello`);
    }
    this.age = age;
    // властивість функції-конструктора (статична властивість)
    Human.countUsers = 0;
}

// метод екземпляру винесений на прототип
Human.prototype.say = function() {
    console.log((`Hello ${this.name}`));
}

// Створили масив
const peoples = new Array();
// Створили кнопку додавання користівачів
document.getElementById("add-user").onclick = () => {
    let age = prompt("Введіть кількість років: ", "вік");
    let name = prompt("Введіть ім'я: ", "ім'я");
    peoples.push(new Human(age, name));
    console.log(peoples);
    Human.say();
}
// створили кнопку сортування користувачів за віком
document.getElementById("sort-user").onclick = () => {
    peoples.sort((a, b) => a.age - b.age);
    peoples.forEach((item) => document.write(`${item.name} : ${item.age} <br>`));
}






