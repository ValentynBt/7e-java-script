`use strict`
const date = `© 2002 – ${new Date().getFullYear()} Мережа піцерій Domino!`;
// Об'єкт для зберігання даних замовника
const user = {
    name: "",
    phone: "",
    email: "",
}

window.addEventListener(`DOMContentLoaded`, () => {
    document.getElementById("address").innerText = date;
    const radioInp = document.querySelector(`#pizza`),
        price = document.querySelector(`.price`),
        priceText = document.createElement(`p`),
        // Соуси і топінги(перетягуванні елементи)
        ingrid = document.querySelector(`.ingridients`),
        table = document.querySelector(`.table`),
        // Контент блоків соуси і топінги
        sauces = document.querySelector(`.sauces`),
        topings = document.querySelector(`.topings`),
        // Елементи форми
        formInp = document.querySelector(`.grid`),
        inpName = document.querySelector(`.inp-name`),
        inpTell = document.querySelector(`.inp-tell`),
        inpMail = document.querySelector(`.inp-mail`),
        // Банер
        banner = document.querySelector(`#banner`);

    // Радіо-перемекачі
    let itemId;
    let checkSizePizza = pizza.big;
    priceSize(priceText, pizza.big, price);
    radioInp.addEventListener(`click`, (e) => {
        if(e.target.value == `small`) {
            if(itemId != 0) {
                priceText.textContent = (Number(priceText.textContent) - Number(checkSizePizza)) + Number(pizza.small);
                checkSizePizza = pizza.small;
            } else {
                checkSizePizza = priceSize(priceText, pizza.small, price);
            }
        } else if(e.target.value == `mid`) {
            if(itemId != 0) {
                priceText.textContent = (Number(priceText.textContent) - Number(checkSizePizza)) + Number(pizza.middle);
                checkSizePizza = pizza.middle;
            } else {
                checkSizePizza = priceSize(priceText, pizza.middle, price);
            }
        } else if(e.target.value == `big`) {
            if(itemId != 0) {
                priceText.textContent = (Number(priceText.textContent) - Number(checkSizePizza)) + Number(pizza.big);
                checkSizePizza = pizza.big;
            } else {
                checkSizePizza = priceSize(priceText, pizza.big, price);
            }
        }
    })
    // Поля вводу данних замовника
    formInp.addEventListener(`click`, (e) => {
        // Перевірка введених данних замовника
        inpName.addEventListener(`change`, () => {
            if(checkExp.nameRegExp.test(inpName.value) || inpName.value == ``) {
                checkInp.checkName = false;
                inpName.classList.add("error");
                inpName.classList.remove("success");
            } else {
                user.name = inpName.value.toLowerCase();
                inpName.classList.add("success");
                inpName.classList.remove("error");
                checkInp.checkName = true;
            }
        })
        inpTell.addEventListener(`change`, () => {
            if(/^\+380[0-9]{9}$/.test(inpTell.value)) {
                user.phone = inpTell.value.toLowerCase();
                inpTell.classList.add("success");
                inpTell.classList.remove("error");
            } else {
                inpTell.classList.add("error");
                inpTell.classList.remove("success");
            }
        })
        inpMail.addEventListener(`change`, () => {
            if(/^[A-z_0-9.]+@[A-z-]+\.[A-z]{1,4}\.?[A-z]*$/.test(inpMail.value)) {
                user.email = inpMail.value.toLowerCase();
                inpMail.classList.add("success");
                inpMail.classList.remove("error");
            } else {
                inpMail.classList.add("error");
                inpMail.classList.remove("success");
            }
        })
        // Перевірка інпутів після натиску кнопки підтвердження
        if(e.target.value == `Підтвердити замовлення >>`) {
            if(checkExp.nameRegExp.test(inpName.value) || inpName.value == ``) {
                checkInp.checkName = false;
                console.log(`red`);
            } 
            if(checkInp.checkName == false) {
                console.log(`red`);
            } else {
                // Записуємо в Local Storage замовлену відвідувачем піцу
                switch(checkSizePizza) {
                    case `100`:
                        user.pizzaSize = `small`;
                        break;
                    case `150`:
                        user.pizzaSize = `middle`;
                        break;
                    case `200`:
                        user.pizzaSize = `big`;
                        break;
                }

                let [...userIngrid]  = document.querySelectorAll(`.ingridient`);
                user.ingridients = userIngrid.map((paragraph) => {
                    return paragraph.innerText;
                })
                // Записуємо в Local Storage дані замовника
                localStorage.setItem(`user`, JSON.stringify(user));
                console.log(localStorage.user);
                checkBtn(inpName.value, inpTell.value, inpMail.value);
            }
        }
    });
    // Тікаючий банер
    banner.addEventListener(`mouseover`, (e) => {
        e.target.style.bottom = `${Math.floor(Math.random()*90)}%`;
        e.target.style.right = `${Math.floor(Math.random()*90)}%`;
        console.log(`${e.clientX} / ${e.clientY}`);
    })
    // ФУНКЦІЯ Drag & Drop
    ingrid.addEventListener(`dragstart`, function(evt) {
        // змінюємо стиль в початку операції drag & drop
        // evt.target.style.border = "3px dotted hsl(186, 100%, 69%)";
        // Свойство effectAllowed управляє візуальним ефектом (частіше за всього це вид вказівника миші), який браузер створює на відповідь 
        // на тип створюваної події перетягування (перетягування, копіювання и т.д.).
        evt.dataTransfer.effectAllowed = "move";
        // Метод setData(...) сповіщає механізм перетягування в браузері, які данні з перетягуємого об'єкта повинен «піймать»
        // цільвий елемент, ще іменований як "зона прийняття". Тут ми вказуєм, що переданні данні це id элемента.
        // "Text" - це тип данних перетягування для додавання до перетягуємого об'кту.
        evt.dataTransfer.setData("id", evt.target.id);
    }, false)
    // видаляєм стилі додані спочатку операції drag & drop
    ingrid.addEventListener("dragend", function(evt) {
        evt.target.style.border = "";  
    }, false);
    // перетягуємий об'єкт попадає в область цільового елемента
    table.addEventListener("dragenter", function(evt) {
        // evt.target.style.border = "3px solid red";
    }, false);
    // перетягуємий елемент покидає область цільового елемента
    table.addEventListener("dragleave", function(evt) {
        evt.target.style.border = "";
    }, false);
    // відміняєм стандартний обработчик події dragover.
    table.addEventListener("dragover", function(evt) {
        // реалізація данного обработчика за замовчіванням не дозволяє перетягувати данні на цільовий елемент, так як більша
        // частина веб-сорінки не може приймати данні.
        // Для того щоб елемент зміг прийняти перетягуємі данні необхідно відмінити стандартний обробник.
        if (evt.preventDefault) evt.preventDefault();
            return false;
    }, false);
    // перетягуємий елемент відпущен над цільовим елементом
    table.addEventListener("drop", function(evt) {
        // зупиняєм подальше розповсюдження події по дереву DOM і відміняєм можливий стандартний обробник встановлений браузером.
        if (evt.preventDefault) evt.preventDefault();
        if (evt.stopPropagation) evt.stopPropagation();
        // отримуєм інформацію яка передавалась через операцію drag & drop
        this.style.border = "";
        itemId = evt.dataTransfer.getData("id");
        console.log(itemId);
        let elemIngr = document.getElementById(itemId);
        // клонуєм елемент в цільовий елемент. Так як в DOM не може бути два однакових елемента - елемент видаляється зі своєї старої позиції.
        let cloneelemIngr = elemIngr.cloneNode(true);
        evt.target.after(cloneelemIngr);

        // Додавання соусів, топінгів і ціни
        let ingr = document.createElement(`p`);
        ingr.classList.add(`ingridient`);
        ingr.style.fontSize = `24px`;
        ingr.style.color = `hsl(30, 100%, 69%)`;
        // Кнопка хрестик
        let btnCross = document.createElement(`button`);
        btnCross.textContent = `X`;
        btnCross.style.cursor = `pointer`;
        btnCross.style.marginLeft = `10px`;
        btnCross.style.width = `20px`;
        btnCross.style.height = `25px`;
        // Сортування соусів і топінгів
        if(itemId in nameIngr) {
            if(/sauce/g.test(itemId)) {
                ingr.textContent = nameIngr[itemId];
                sauces.append(ingr);
                ingr.append(btnCross);
            } else {
                ingr.textContent = nameIngr[itemId];
                topings.append(ingr);
                ingr.append(btnCross);
            }
        }
        // Зміна ціни
        if(itemId in ingridPrice) {
            priceText.textContent = Number(priceText.textContent) + Number(ingridPrice[itemId]);
            btnCross.value = ingridPrice[itemId];
        }
        // Видалення інгрідієнтів через хрестик
        btnCross.addEventListener(`click`, () => {
            priceText.textContent -= btnCross.value;
            ingr.remove();
            cloneelemIngr.remove();
        })
        return false;
    }, false)

})

// Об'єкт ціни піци за її розміром
const pizza = {
    small: `100`,
    middle: `150`,
    big: `200`,
}
// Об'єкт для перевірки правильно введеного ім'я замовника
const checkInp = {
    checkName: null,
}
// Об'єкт регулярних виразів для перевірки введенних данних замовника
const checkExp = {
    nameRegExp: /[ !@#$&*0-9]/g,
    tellRegExp: /\+\d{12}/g,
    mailRegExp: /\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/i,
}
// Об'єкт інградієнтів
const ingridPrice = {
    // Соуси
    sauceClassic: `5`,
    sauceBBQ: `10`,
    sauceRikotta: `15`,
    // Топінги
    moc1: `20`,
    moc2: `25`,
    moc3: `30`,
    telya: `40`,
    vetch1: `20`,
    vetch2: `30`,
}
// Наіменування інгрідієнтів по ID 
const nameIngr = {
    sauceClassic: `Кетчуп`,
    sauceBBQ: `BBQ`,
    sauceRikotta: `Рiкотта`,
    moc1: `Сир звичайний`,
    moc2: `Сир фета`,
    moc3: `Моцарелла`,
    telya: `Телятина`,
    vetch1: `Помiдори`,
    vetch2: `Гриби`,
}
// Функція виводу ціни за розміром
function priceSize(el, size, price) {
    el.textContent = size;
    el.style.color = `hsl(11, 100%, 69%)`;
    price.append(el);
    return checkSizePizza = size;
}
// Функція для перевірки введених данних замовника
function checkBtn(name, tell, mail) {
    if((checkExp.nameRegExp.test(name) || name == ``) ||
        (!checkExp.tellRegExp.test(tell) || tell.length > 13) ||
        !checkExp.mailRegExp.test(mail)) {
            
            return console.log(`false`);
    } else {
        setTimeout(() => {
            window.location = `./thank-you.html`;
        }, 100);
    }
}


