"use strict"

// Задача 1

function click() {
    let array = [];
    let numberArr;
    do {
        numberArr = parseFloat(prompt("Введіть числа для підрахунку суми всіх введених чісел: ", `число`));
        if(Number.isFinite(numberArr)) {
            array.push(numberArr);
        }
    } while (Number.isFinite(numberArr));
    console.log(array);
    
    function fn (theArr) {
        let summArr = 0;
        for(let i of theArr) {
            summArr += i;
        }
        return summArr;
    }
    function map(callback, anyArray) {
        document.getElementById("display").innerHTML = callback(anyArray);
    }
    map(fn, array);
}
const btnStart = document.getElementById("btn-start");
btnStart.onclick = click;

// Задача 2

function checkAge(Age) {
    return (Age > 18) ? true : confirm(`Батьки дозволили?`)
}

