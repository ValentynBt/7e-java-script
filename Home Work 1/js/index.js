"use strict"

// Задача 2
// 1)
let x = 6, y = 14, z = 4;

document.write((x += y - x++ * z) + "<br>");
// Відповідь -4

// Послідовність виконання розрахунку по пріорітетності операторів:
// 1. Постфіксний інкрімент (пріорітет 16): х++ (тобто 6);
// 2. Множення (пріорітет 13): 6 * 4 = 24;
// 3. Віднімання (пріорітет 12): 14 - 24 = -10;
// 4  Присваювання += (пріорітет 2) (асоціативність зправа наліво): 6 + (-10) = -4;

// 2)
x = 6, y = 14, z = 4;
document.write((z = --x - y * 5) + "<br>");
// Відповідь -65

// Послідовність виконання розрахунку по пріорітетності операторів:
// 1. Префіксний дікрімент (пріорітет 15): --х (тобто 5);
// 2. Множення (пріорітет 13): 14 * 5 = 70;
// 3. Віднімання (пріорітет 12): 5 -70 = -65;
// 4. Присваювання (пріорітет 2): -65

// 3)
x = 6, y = 14, z = 4;
document.write((y /= x + 5 % z) + "<br>");
// Відповідь 2

// Послідовність виконання розрахунку по пріорітетності операторів:
// 1. Залишок від ділення (пріорітет 13): 5 % 4 = 1 ;
// 2. Додавання (пріорітет 12): 6 + 1 = 7;
// 3. Присваювання /= (пріорітет 2): 14 / 7 = 2

// 4)
x = 6, y = 14, z = 4;
document.write((z - x++ + y * 5) + "<br>");
// Відповідь 68

// Послідовність виконання розрахунку по пріорітетності операторів:
// 1. Постфіксний інкрімент (пріорітет 16): 6
// 2. Множення (пріорітет 13): 14 * 5 = 70
// 3. Віднімання / додавання (пріорітет 12) асоціативність зліва направо: 4 - 6 + 70 = 68
// 4. Присваювання (пріорітет 2): 68

// 5)
x = 6, y = 14, z = 4;
document.write((x = y - x++ * z) + "<br>")
// Відповідь -10

// Послідовність виконання розрахунку по пріорітетності операторів:
// 1. Постфіксний інкрімент (пріорітет 16): 6
// 2. Множення (пріорітет 13): 6 * 4 =24
// 3. Віднімання (пріорітет 12): 14 - 24 = -10
// 4. Присваювання (пріорітет 2): -10
