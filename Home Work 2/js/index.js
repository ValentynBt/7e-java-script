"use strict"

// Задача 1

let userRows;
do {
    userRows = parseFloat(prompt(`Введіть ціле число висоту фігурки: `, `ціле число`));
} while (userRows / parseInt(userRows) != 1);

document.write(`Користувач загадав висоту: ${userRows} <br>`);

// Трикутник
for (let i = 0; i <= userRows; i++) {
    for (let j = (1 + i); j <= userRows; j++) {
        document.write("&nbsp"+"&nbsp");
    }
    for (let k = i; k >= 0; k--) {
        document.write("*");
    }
    for (let l = (userRows - i); l < userRows; l++) {
        document.write("*");
    }
    document.write("<br>");
}
// Ромб
for (let i = 0; i <= userRows; i++) {
    for (let j = (1 + i); j <= userRows; j++) {
        document.write("&nbsp"+"&nbsp");
    }
    for (let k = i; k >= 0; k--) {
        document.write("*");
    }
    for (let l = (userRows - i); l < userRows; l++) {
        document.write("*");
    }
    document.write("<br>");
}
for (let i = 0; i <= userRows; i++) {
    for (let j = (userRows - i); j <= userRows; j++) {
        document.write("&nbsp"+"&nbsp");
    }
    for (let k = (i + 1); k <= userRows; k++) {
        document.write("*");
    }
    for (let l = (i + 2); l <= userRows; l++) {
        document.write("*");
    }
    document.write("<br>");
}
// Прямокутник
for (let i = 0; i < userRows; i++) {
    for (let j = 0; j <= userRows; j++) {
        if ((j > 0 && j <= userRows - 1) && (i > 0 && i < userRows - 1)) {
            document.write("&nbsp"+"&nbsp");
        } else {
            document.write("*");
        }
    }
    document.write("<br>");
}

// Задача 2

let number;
do {
    number = parseFloat(prompt(`Введіть ціле число: `, `ціле число`));
} while (number / parseInt(number) != 1);

console.log(`Користувач загадав число: ${number}`);

for(let i = 0; i <= number; i++) {
    if(i % 5 == 0) {
        console.log(i);
    } else if(number < 5) {
        console.log(`Sorry, no numbers`);
        break;
    }
}
for(let i = 0; i >= number; i--) {
    if(i % 5 == 0) {
        console.log(i);
    } else if(number > - 5) {
        console.log(`Sorry, no numbers`);
        break;
    }
}    




