`use strict`

/* У папці calculator дана верстка макета калькулятора. Необхідно зробити цей калькулятор робітником.<br>
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.<br>
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.<br>
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.<br>
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.*/

window.addEventListener("DOMContentLoaded", () => {
    const btn = document.querySelector(".keys"),
        display = document.querySelector(".display > input"),
        btnequals = document.querySelector(`.orange`);

    btn.addEventListener("click", (e) => {

        if(/[C]/.test(e.target.value)) {
            remove(calc);
        }

        if(/[(\+\)(\-\)(*)(/)]/.test(e.target.value) && calc.operator == ``) {
            calc.operator = e.target.value;
            if(calc.operator == `m+`) {
                calc.operator = ``;
            }
        }

        if(calc.operator == `` && e.target.value != `m+` && e.target.value != `mrc`) {
            calc.value1 += e.target.value;
            if(/[C]/.test(e.target.value)) {
                remove(calc);
            }
            show(calc.value1, display);
        } else if(!/[(+)(\-\)(*)(/)(=)(\C)(mrc)]/.test(e.target.value)) {
            calc.value2 += e.target.value;
            show(calc.value2, display);
            btnequals.removeAttribute(`disabled`, true);
        }

        if(/[=]/.test(e.target.value)) {
            calc.result = calculate(parseFloat(calc.value1), parseFloat(calc.value2), calc.operator);
            show(calc.result, display);
            
            calc.value1 = calc.result, calc.value2 = ``, calc.operator = ``;
        }
        if(calc.value1 != `` && calc.value2 != `` && /[(+)(\-\)(*)(/)]/.test(e.target.value) && e.target.value != `m+`) {
            calc.result = calculate(parseFloat(calc.value1), parseFloat(calc.value2), calc.operator);
            show(calc.result, display);
            
            calc.value1 = calc.result;
            calc.value2 = ``;
            calc.operator = e.target.value;
        }

        if(e.target.value == `m+`) {
            mrc.mPlus = display.value;
            console.log(mrc.mPlus);
        }
        if(/(m-)/.test(e.target.value)) {
            mrc.mPlus = ``;
            show(mrc.mPlus, display);
        }
        if(e.target.value == `mrc`) {
            display.value = mrc.mPlus;
        }
    })
});

const calc = {
    value1: "",
    value2: "",
    operator: "",
    result: "",
};
let mrc = {
    mPlus: ``,
}

function show (value, el) {
    el.value = value;
};
function remove(objCalc) {
    for(key in objCalc) {
        objCalc[key] = ``;
    }
    return objCalc
}
// Тіло калькулятора
let sum = (a, b) => { return a + b; };
let subtruction = (a, b) => { return a - b; };
let divide = (a, b) => {
    if(b === 0) {
        alert(`На 0 не можна ділить`);
    }
    return a / b;
}
let multiply = (a, b) => { return a * b; };

function calculate(a, b, oper) {
    switch(oper) {
        case `+` :
            return sum(a, b);
        case `-` :
            return subtruction(a, b);
        case `*` :
            return multiply(a, b);
        case `/` :
            return divide(a, b);
    };
};
