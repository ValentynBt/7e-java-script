`use strict`
/* 2.
Дано інпути. Зробіть так, щоб усі інпути після втрати фокусу перевіряли свій вміст на правильну кількість символів. 
Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість, 
то межа інпуту стає зеленою, якщо неправильна – червоною. */

window.addEventListener(`DOMContentLoaded`, () => {
    const [...inputs] = document.querySelectorAll(`input`);
    inputs.forEach((element) => {
        element.addEventListener(`change`, (e) => {
            if(e.target.value.length <= e.target.dataset.length) {
                console.log(e.target.value);
                e.target.style.border = `3px solid green`;
            } else {
                e.target.style.border = `3px solid red`;
            }
        })
    }) 
    
})