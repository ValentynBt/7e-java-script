import city from "./uacity.js";

/* 3.
Використовуючи бібліотеку bootstrap створіть форму у якій запитати у користувача данні.
Ім'я, Прізвище (Українською)
Список з містами України 
Номер телефону у форматі +380XX XXX XX XX - Визначити код оператора та підтягувати логотип оператора. 
Пошта 
Якщо поле має помилку показати червоний хрестик  біля поля ❌,  якщо помилки немає показати зелену галочку ✅

Перевіряти данні на етапі втрати фокуса та коли йде натискання кнопки відправити дані */

const dataCity = document.querySelector(`#data-city`);

city.forEach((element) => {
    const option = document.createElement(`option`);
    option.value = element.city;
    dataCity.append(option);
})
// Шукаєм інпути
const userName = document.querySelector(`#name`),
    userLastName = document.querySelector(`#lastName`),
    userCity = document.querySelector(`#city`),
    userPhone = document.querySelector(`#tel`),
    userMail = document.querySelector(`#mail`),
    button = document.querySelector(`#btn-send`);

function validate(patern, value) {
    return patern.test(value);
}
// Перевірка інпутів
userName.addEventListener(`change`, (e) => {
    const span = document.querySelector(`#n`);
    if(validate(/^[А-я-ІіЇїЄє]+$/, e.target.value)) {
        span.textContent = `✅`;
    } else {
        span.textContent = `❌`;
    }
})
userLastName.addEventListener(`change`, (e) => {
    const span = document.querySelector(`#ln`);
    if(validate(/^[А-я-ІіЇїЄє]+$/, e.target.value)) {
        span.textContent = `✅`;
    } else {
        span.textContent = `❌`;
    }
})
userCity.addEventListener(`change`, (e) => {
    const span = document.querySelector(`#ct`);
    if(validate(/[\W[0-9]/, e.target.value)) {
        span.textContent = `❌`;
    } else {
        span.textContent = `✅`;
    }
})
userPhone.addEventListener(`change`, (e) => {
    const span = document.querySelector(`#tl`);
    const paternPhone = e.target.value;
    if(validate(/^\+380\d{2} \d{3} \d{2} \d{2}$/, e.target.value)) {
        span.textContent = `✅`;
        let operPhone = document.createElement(`img`);
        
        let operNumb = paternPhone.slice(4, 6);
        if(operNumb == 96 || operNumb == 67) {
            operPhone.setAttribute(`src`, `image/kyivstar-logo.png`);
            span.after(operPhone);
        }
        if(operNumb == 73 || operNumb == 93 || operNumb == 96) {
            operPhone.setAttribute(`src`, `image/lifecell-logo.jpg`);
            span.after(operPhone);
        }
        if(operNumb == 50) {
            operPhone.setAttribute(`src`, `image/vodafon-logo.jpg`);
            span.after(operPhone);
        }
    } else {
        span.textContent = `❌`;
    }
})
userMail.addEventListener(`change`, (e) => {
    const span = document.querySelector(`#ml`);
    if(validate(/\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/i, e.target.value)) {
        span.textContent = `✅`;
    } else {
        span.textContent = `❌`;
    }
})
button.addEventListener(`click`, (e) => {
    if(document.querySelector(`#n`).textContent == `✅` &&
        document.querySelector(`#ln`).textContent == `✅` &&
        document.querySelector(`#ct`).textContent == `✅` &&
        document.querySelector(`#tl`).textContent == `✅` &&
        document.querySelector(`#ml`).textContent == `✅`) {
            setTimeout(() => {
                document.location = `https://www.meme-arsenal.com/create/meme/6244708`;
            }, 300);
        } else {
            console.error(`no signal`);
        }
})