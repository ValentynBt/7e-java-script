/* 4.
- При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
- Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: 
. 'Поточна ціна: ${значення з поля введення}`
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
- під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється. */

const data = {
    inputNumber: document.querySelector(`#input`),
    span: document.createElement(`span`),
    btn: document.createElement(`button`),
    value: ``,
    error: document.createElement(`span`),
}
data.btn.innerText = `X`;

data.btn.addEventListener(`click`, () => {
    data.span.remove();
    data.inputNumber.classList.remove(`error`, `active`);
    data.inputNumber.value = ``;
})
data.inputNumber.addEventListener(`focus`, () => {
    data.inputNumber.classList.add(`active`);
})
data.inputNumber.addEventListener(`change`, () => {
    if(data.inputNumber.value < 0) {
        data.inputNumber.classList.add(`error`);
        data.error.innerText = `Please enter correct price`;
        data.inputNumber.after(data.error);
        data.span.remove();
        return
    }
    data.value = data.inputNumber.value;
    data.inputNumber.classList.remove(`active`);
    data.inputNumber.classList.remove(`error`);
    data.span.innerText = `Поточна ціна ${data.value}`;
    data.span.append(data.btn);
    data.inputNumber.before(data.span);
    data.error.innerText = ``;
    
})